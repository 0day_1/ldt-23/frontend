import config from '../config/UrlConfig';

const base64 = require('base-64');
const apiUrl = config.apiUrl;

class Authorization {

    getUserByUsernameAndPassword = async (username, password) => {
        const credentials = `${username}:${password}`;
        const encodedCredentials = base64.encode(credentials);
        
        try {
          const response = await fetch(`${apiUrl}/auth/api/v1/user`, {
            method: 'GET',
            headers: {
              'Authorization': `Basic ${encodedCredentials}`
            },
          });
      
          if (response.ok) {
            const user = await response.json();
            return user.username;
          } else {
            throw new Error('Request failed with status: ' + response.status);
          }
        } catch (error) {
          console.error('Error:', error.message);
          return null;
        }
      };
      

}

export default Authorization;