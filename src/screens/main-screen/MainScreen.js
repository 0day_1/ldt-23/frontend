import React from 'react';
import TopBar from '../../components/top-bar/TopBar';
import Filters from '../../components/filter/Filter';
import Card from '../../components/card/Card';
import './MainScreen.css';

import Image from '../../assets/example.jpg';

const MainScreen = () => {

    const generateCards = (num) => {
      const cards = [];
      for (let i = 1; i <= num; i++) {
        cards.push({
          id: i,
          title: `Заведение ${i}`,
          description: `Описание: Cards assume no specific width to start, so theyll be 100% wide unless otherwise stated.`,
          image: Image,
          address: `Адрес: ${i}`,
          hours: `Часы работы: ${i}`,
          reviews: `Отзывы: ${i * 5}`,
          price: `Цена: ${i} $`,
        });
      }
      return cards;
    };
  
    const cardsData = generateCards(26);
  
    return (
      <div className="site-bar">
        {<TopBar/>}
        <div className="row main-screen">
          <div className="col-md-3 left-column">
            {<Filters/>}
          </div>
          <div className="col-md-9 right-column py-4">
              {cardsData.map((card) => (
                <Card
                  key={card.id}
                  title={card.title}
                  description={card.description}
                  image={card.image}
                  address={card.address}
                  hours={card.hours}
                  reviews={card.reviews}
                  price={card.price}
                />
              ))}
          </div>
        </div>
    </div>
    );
  };
  
  export default MainScreen;