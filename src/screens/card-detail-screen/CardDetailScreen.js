import React, { useState } from 'react';
import TopBar from '../../components/top-bar/TopBar';

const CardDetailScreen = ({ title, description, image, hours, reviews, price }) => {
  return (
    <div>
    {<TopBar/>}
        <div className="object-details">
        <h2>{title}</h2>
        <div className="description">{description}</div>
        <img src={""} alt="Изображение заведения" />
        <div className="hours">Часы работы: {hours}</div>
        <div className="reviews">Отзывы: {reviews}</div>
        <div className="price">Цена: {price}</div>
        </div>
    </div>
  );
};

export default CardDetailScreen;