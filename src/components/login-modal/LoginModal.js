import React, { useState } from 'react';
import Modal from 'react-modal';
import './LoginModal.css';
import { FaTimes } from 'react-icons/fa';
import Authorization from '../../service/Authorization';
import RegistrationModal from '../../components/registration-modal/RegistrationModal';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

Modal.setAppElement('#root');

const LoginModal = ({ closeModal }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);
  const authorization = new Authorization();

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleRememberMeChange = (event) => {
    setRememberMe(event.target.checked);
  };

  //Авторизация
  async function signIn () {
    console.log('Логин:', username);
    console.log('Пароль:', password);
    console.log('Запомнить меня:', rememberMe);
    toast.success('User data:');
    try {
      const userData = await authorization.getUserByUsernameAndPassword(username, password);
      toast.success('User data:', userData);
      console.log('User data:', userData);
    } catch (error) {
      toast.success('Error:', error);
      console.error('Error:', error);
    }
    // closeModal(); 
  };

  return (
    <div>
      <Modal
        isOpen={true} 
        onRequestClose={closeModal}
        className="modal"
        overlayClassName="overlay"
      >
        <div className="close-icon" onClick={closeModal}>
          <FaTimes />
        </div>
        <h2>Авторизация</h2>
        <form>
          <div className="form-group-login">
            <label htmlFor="username">Логин:</label>
            <input className="form-control mb-2"
              type="username"
              id="username"
              value={username}
              onChange={handleUsernameChange}
            />
            <label htmlFor="password">Пароль:</label>
            <input className="form-control mb-2"
              type="password"
              id="password"
              value={password}
              onChange={handlePasswordChange}
            />
            <label>
              <input className="mb-2"
                type="checkbox"
                checked={rememberMe}
                onChange={handleRememberMeChange}
              />
              Запомнить меня
            </label>
          </div>
          <div className="form-group">
            <a className="col px-0 text-center" href="#forget-password">Забыли пароль</a>
            <button type="submit" className="btn btn-success mt-1 mx-0" onClick={signIn}>Войти</button>
            <button type="button" className="btn btn-outline-primary btn-block mt-1 mx-0" onClick={"#registration"}>Регистрация</button>
          </div>
          </form>
      </Modal>
    </div>
  );
};

export default LoginModal;
