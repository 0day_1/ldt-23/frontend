import React, { useState } from 'react';
import { FaSearch } from 'react-icons/fa';
import './Filter.css';

const Filters = ({ onSearch, onReset }) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [price, setPrice] = useState('');
  const [area, setArea] = useState('');
  const [buildingType, setBuildingType] = useState('');

  const handleSearchQueryChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };

  const handleAreaChange = (event) => {
    setArea(event.target.value);
  };

  const handleBuildingTypeChange = (event) => {
    setBuildingType(event.target.value);
  };

  const handleSearch = () => {
    const filters = {
      searchQuery,
      price,
      area,
      buildingType,
    };

    onSearch(filters);
  };

  const handleReset = () => {
    setSearchQuery('');
    setPrice('');
    setArea('');
    setBuildingType('');

    onReset();
  };

  return (
    <div className="row justify-content-center">
      
      <div>
      <h2 className="text-center my-3">Фильтры</h2>
        <div className="form-group">
          <label htmlFor="searchQuery">Поиск по названию:</label>
          <input className="form-control"
            type="text"
            id="searchQuery"
            value={searchQuery}
            onChange={handleSearchQueryChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="price">Цена:</label>
          <input className="form-control"
            type="text"
            id="price"
            value={price}
            onChange={handlePriceChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="area">Площадь кв. м.:</label>
          <input className="form-control"
            type="text"
            id="area"
            value={area}
            onChange={handleAreaChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="buildingType">Тип здания:</label>
          <select className="custom-select"
            id="buildingType"
            value={buildingType}
            onChange={handleBuildingTypeChange}
          >
            <option defaultValue="" disabled>Все</option>
            <option value="house">Дом</option>
            <option value="apartment">Квартира</option>
            <option value="office">Офис</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="buildingType">Расположение:</label>
          <select className="custom-select"
            id="locations"
            value={buildingType}
            onChange={handleBuildingTypeChange}
          >
            <option defaultValue="" disabled>Все</option>
            <option value="irk">Иркутск</option>
            <option value="mng">Монголия</option>
            <option value="srb">Сербия</option>
          </select>
        </div>
        <div className="row justufy-content-center mt-4">
          <button onClick={handleReset} type="button" className="col btn btn-outline-secondary">Сбросить</button>
          <button onClick={handleSearch} type="submit" className="col btn btn-success mx-0">Поиск</button>
        </div>
      </div>
    </div>
  );
};

export default Filters;
