import React, { useState } from 'react';
import Modal from 'react-modal';
import './RegistrationModal.css';
import { FaTimes } from 'react-icons/fa';

import Authorization from '../../service/Authorization';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

Modal.setAppElement('#root');

const RegistrationModal = ({ closeModal }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);
  const authorization = new Authorization();

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleRememberMeChange = (event) => {
    setRememberMe(event.target.checked);
  };

  //Авторизация
  async function signIn () {
    try {
      const userData = await authorization.getUserByUsernameAndPassword(username, password);
      toast.success('User data:', userData);
      console.log('User data:', userData);
    } catch (error) {
      toast.success('Error:', error);
      console.error('Error:', error);
    }
    // closeModal(); 
  };

  return (
    <div>
      <Modal
        isOpen={true} 
        onRequestClose={closeModal}
        className="modal"
        overlayClassName="overlay"
      >
        <div className="close-icon" onClick={closeModal}>
          <FaTimes />
        </div>
        <h2>Регистрация</h2>
        <form>
          <div className="form-group-registration">
            <label htmlFor="username">Логин:</label>
            <input
              type="username"
              id="username"
              value={username}
              onChange={handleUsernameChange}
            />
          </div>
          <div className="form-group-registration">
            <label htmlFor="password">Пароль:</label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={handlePasswordChange}
            />
          </div>
          <div className="form-group-registration">
            <label htmlFor="role">Роль:</label>
            <input
              type="role"
              id="role"
              onChange={handlePasswordChange}
            />
          </div>
          <div className="form-group-registration">
          <a href="#authorization">Авторизация</a>
          </div>
          <div className="form-group-registration">
          <a href="#forget-password">Забыли пароль</a>
          </div>
          <button type="registration" onClick={signIn}>Зарегистрироваться</button>
        </form>
      </Modal>
    </div>
  );
};

export default RegistrationModal;
