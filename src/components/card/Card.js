import React, { useState } from 'react';
import CardDetailScreen from '../../screens/card-detail-screen/CardDetailScreen';
import './Card.css';

const Card = ({ title, description, image, address, hours, reviews, price }) => {
  return (

    <div className="card">
      <div className="card-body">
        <h5 className="card-title text-center">{title}</h5>
        <img src={image} alt={title} className="card-image" />
        {/* <div className="container"> */}
          <div className="address">{address}</div>
          <div className="hours">{hours}</div>
          <div className="price">{price}</div>
          <div className="reviews">{reviews}</div>
          {/* <div className="description">{description}</div> */}
        {/* </div> */}
      </div>
      <div className="container justify-content-center my-3">
        <button type="submit" className="col btn btn-success mx-0">Глянуть</button>
      </div>
    </div>
  );
};

{/*<div className="object-card">
      <div className="title">
        <label>{title}</label>
      </div>
      <div className="slider">
        {/* Код для слайдера картинок /}
        <img src={image} alt={title} className="card-image" />
      </div>
      <div className="details">
        <div className="address">{address}</div>
        <div className="hours">{hours}</div>
        <div className="description">{description}</div>
      </div>
      <div className="footer">
        <div className="reviews">{reviews}</div>
        <div className="price">{price}</div>
      </div>
      </div> */}

export default Card;
