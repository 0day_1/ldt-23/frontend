import React, { useState } from 'react';
import { FaUser, FaTelegram, FaVk } from 'react-icons/fa';
import './TopBar.css';
import LoginModal from '../login-modal/LoginModal';

import logo from '../../hacklogo.PNG';

const TopBar = () => {

    const [isModalOpen, setIsModalOpen] = useState(false);

    const openModal = () => {
      setIsModalOpen(true);
    };
  
    const closeModal = () => {
      setIsModalOpen(false);
    };

    return (
        <header>
            <nav className="navbar navbar-expand-lg mx-5 px-3">
                <h1 className="navbar-brand title mx-0 my-0" href="#">
                    <img src={logo} alt="Логотип"/>   
                </h1>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse justify-content-md-end" id="navbarNavAltMarkup">
                    <div className="navbar-nav header-link">
                        <a className="nav-item nav-link mx-3 align-items-center" href="#home">Главная</a>
                        <a className="nav-item nav-link mx-3 align-items-center" href="#directory">Справочник</a>
                        <a className="row nav-item mr-2 ml-3 align-items-center" href="https://t.me/i_alakey"><FaTelegram /></a>
                        <a className="row nav-item ml-2 mr-3 align-items-center" href="https://vk.com/i_alakey"><FaVk /></a>
                        <a className="row nav-item ml-2 align-items-center" href="#login" onClick={openModal}><FaUser className="mr-1"></FaUser>Войти</a>
                        {isModalOpen && <LoginModal closeModal={closeModal}/>}
                    </div>
                </div>
            </nav>
        </header>
    );
};

export default TopBar;
