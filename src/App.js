import React from 'react';
import MainScreen from './screens/main-screen/MainScreen';

const App = () => {

  return (
    <div className="font-face-gm">
      <MainScreen/>
    </div>
  );
};

export default App;
